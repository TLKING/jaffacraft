package ch.TLKING.jaffacraft;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class JaffaCreativeTab {

  public static CreativeTabs jaffaTab = new CreativeTabs("jaffaCraftTab") {
                                        public Item getTabIconItem() {
                                          return JaffaCraft.jaffaCake;
                                        }
                                      };
}
