package ch.TLKING.jaffacraft.items;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import cpw.mods.fml.common.registry.GameRegistry;

import ch.TLKING.jaffacraft.JaffaCraft;
import ch.TLKING.jaffacraft.JaffaCreativeTab;

public class ChocolatePowder extends Item {
  private static String name = "chocolatePowder";

  public ChocolatePowder() {
    super(30002);
    this.setUnlocalizedName(name);
    this.setCreativeTab(JaffaCreativeTab.jaffaTab);
    this.setTextureName(JaffaCraft.MODID + ":" + name);
  }

  public static String getName() {
    return name;
  }

  public static void registerRecepies() {
    GameRegistry.addShapelessRecipe(new ItemStack(JaffaCraft.chocolatePowder, 1), new Object[] { JaffaCraft.mortar,
        new ItemStack(Item.dyePowder, 1, 3) });
  }
}
