package ch.TLKING.jaffacraft.items;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;

import ch.TLKING.jaffacraft.JaffaCraft;
import ch.TLKING.jaffacraft.JaffaCreativeTab;

import cpw.mods.fml.common.registry.GameRegistry;

public class JaffaCake extends ItemFood {
  private static String name = "jaffaCake";

  public JaffaCake() {
    // id, healAmount, saturationModifier, isWolfsFavoriteMeat
    super(30000, 4, 0.6F, false);
    this.setUnlocalizedName(name);
    this.setCreativeTab(JaffaCreativeTab.jaffaTab);
    this.setTextureName(JaffaCraft.MODID + ":" + name);
  }

  public static String getName() {
    return name;
  }

  public static void registerRecepies() {
    GameRegistry.addRecipe(new ItemStack(JaffaCraft.jaffaCake, 3), new Object[] { "AAA", "BBB", "CCC", 'A', JaffaCraft.chocolatePowder, 'B',
        JaffaCraft.orange, 'C', JaffaCraft.flour });

    GameRegistry.addShapedRecipe(new ItemStack(JaffaCraft.jaffaCake, 1), new Object[] { "A", "B", "C", 'A', JaffaCraft.chocolatePowder, 'B',
        JaffaCraft.orange, 'C', JaffaCraft.flour });
  }
}
