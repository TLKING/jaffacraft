package ch.TLKING.jaffacraft.items;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.registry.GameRegistry;

import ch.TLKING.jaffacraft.JaffaCraft;
import ch.TLKING.jaffacraft.JaffaCreativeTab;

public class StoneBowl extends Item {
  private static String name = "stoneBowl";

  public StoneBowl() {
    super(30005);
    this.setUnlocalizedName(name);
    this.setCreativeTab(JaffaCreativeTab.jaffaTab);
    this.setTextureName(JaffaCraft.MODID + ":" + name);
  }

  public static String getName() {
    return name;
  }

  public static void registerRecepies() {
    GameRegistry.addShapedRecipe(new ItemStack(JaffaCraft.stoneBowl, 1), new Object[] { "A A", " A ", 'A', Block.cobblestone });
  }

}
