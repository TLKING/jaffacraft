package ch.TLKING.jaffacraft.items;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.registry.GameRegistry;

import ch.TLKING.jaffacraft.JaffaCraft;
import ch.TLKING.jaffacraft.JaffaCreativeTab;

public class Mortar extends Item {
  private static String name = "mortar";

  public Mortar() {
    super(30006);
    this.setUnlocalizedName(name);
    this.setCreativeTab(JaffaCreativeTab.jaffaTab);
    this.setTextureName(JaffaCraft.MODID + ":" + name);

    this.doesContainerItemLeaveCraftingGrid(new ItemStack(JaffaCraft.jaffaCake, 1));
    this.setMaxDamage(50);
  }

  public static String getName() {
    return name;
  }

  @Override
  public boolean doesContainerItemLeaveCraftingGrid(ItemStack par1ItemStack) {
    return false;
  }

  @Override
  public boolean isDamageable() {
    return true;
  }

  public static void registerRecepies() {
    GameRegistry.addShapelessRecipe(new ItemStack(JaffaCraft.mortar, 1), new Object[] { JaffaCraft.stoneBowl, JaffaCraft.pestle });
  }
  
  public void setDamage(ItemStack mortar){
    int damage = this.getDamage(mortar) - 1;
    
    this.setDamage(mortar, damage);
  }
}
