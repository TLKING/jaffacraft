package ch.TLKING.jaffacraft.items;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.registry.GameRegistry;

import ch.TLKING.jaffacraft.JaffaCraft;
import ch.TLKING.jaffacraft.JaffaCreativeTab;

public class Flour extends Item {
  private static String name = "flour";

  public Flour() {
    super(30003);
    this.setUnlocalizedName(name);
    this.setCreativeTab(JaffaCreativeTab.jaffaTab);
    this.setTextureName(JaffaCraft.MODID + ":" + name);
  }

  public static String getName() {
    return name;
  }

  public static void registerRecepies() {
    GameRegistry.addShapelessRecipe(new ItemStack(JaffaCraft.flour, 1), new Object[] { JaffaCraft.mortar, Item.wheat });
  }
}
