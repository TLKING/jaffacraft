package ch.TLKING.jaffacraft.items;

import ch.TLKING.jaffacraft.JaffaCraft;
import ch.TLKING.jaffacraft.JaffaCreativeTab;

import net.minecraft.item.ItemFood;

public class Orange extends ItemFood {
  private static String name = "orange";

  public Orange() {
    // id, healAmount, saturationModifier, isWolfsFavoriteMeat
    super(30001, 4, 0.3F, false);
    this.setUnlocalizedName(name);
    this.setCreativeTab(JaffaCreativeTab.jaffaTab);
    this.setTextureName(JaffaCraft.MODID + ":" + name);
  }

  public static String getName() {
    return name;
  }
}
