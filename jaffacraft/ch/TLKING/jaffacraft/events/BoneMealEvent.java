package ch.TLKING.jaffacraft.events;

import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.player.BonemealEvent;
import ch.TLKING.jaffacraft.JaffaCraft;
import ch.TLKING.jaffacraft.blocks.SaplingOrange;

/**
 * This class is solely to make sure that if bone meal is used on custom sapling
 * that they will grow.
 * 
 * Note: They will grow instantly!
 */
public class BoneMealEvent {
  @ForgeSubscribe
  public void useBonmeal(BonemealEvent event) {
    System.out.println("BONEMEAL");
    if (event.world.getBlockId(event.X, event.Y, event.Z) == JaffaCraft.saplingOrange.blockID) {
      ((SaplingOrange) JaffaCraft.saplingOrange).growTree(event.world, event.X, event.Y, event.Z, event.world.rand);
    }
  }
}
