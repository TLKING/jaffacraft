package ch.TLKING.jaffacraft.blocks;

import static net.minecraftforge.common.EnumPlantType.Plains;

import java.util.List;
import java.util.Random;

import net.minecraft.block.BlockSapling;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.event.terraingen.TerrainGen;

import ch.TLKING.jaffacraft.JaffaCraft;
import ch.TLKING.jaffacraft.JaffaCreativeTab;
import ch.TLKING.jaffacraft.Generation.Tree.WorldGenTreeOrange;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class SaplingOrange extends BlockSapling {
  private Icon          saplingIcon;
  private static String name = "saplingOrange";

  public SaplingOrange() {
    super(3502);
    this.setCreativeTab(JaffaCreativeTab.jaffaTab);
    this.setTextureName(JaffaCraft.MODID + ":" + name);
    this.setUnlocalizedName(name);

    this.setHardness(0.0F);
    this.setStepSound(soundGrassFootstep);
  }

  public static String getName() {
    return name;
  }

  @Override
  @SideOnly(Side.CLIENT)
  public void registerIcons(IconRegister iconRegister) {
    saplingIcon = iconRegister.registerIcon(this.textureName);
  }

  @Override
  @SideOnly(Side.CLIENT)
  public void getSubBlocks(int sappling, CreativeTabs creativeTab, List sapplingList) {
    sapplingList.add(new ItemStack(sappling, 1, 4));
  }

  @Override
  public EnumPlantType getPlantType(World world, int x, int y, int z) {
    return Plains;
  }

  @Override
  @SideOnly(Side.CLIENT)
  public Icon getIcon(int useless, int p_149691_2_) {
    return saplingIcon;
  }

  @Override
  public void growTree(World world, int x, int y, int z, Random random) {
    System.out.println("Grow Tree");
    if (TerrainGen.saplingGrowTree(world, random, x, y, z)) {
      System.out.println("Grow Tree2");
      int l = world.getBlockMetadata(x, y, z) & 7;

      Object object = new WorldGenTreeOrange(true);
      int i1 = 0;
      int j1 = 0;
      boolean flag = false;
      object = new WorldGenTreeOrange(true, 4, 0, 4);

      if (flag) {
        world.setBlock(x + i1, y, z + j1, 0, 0, 4);
        world.setBlock(x + i1 + 1, y, z + j1, 0, 0, 4);
        world.setBlock(x + i1, y, z + j1 + 1, 0, 0, 4);
        world.setBlock(x + i1 + 1, y, z + j1 + 1, 0, 0, 4);
      } else {
        world.setBlock(x, y, z, 0, 0, 4);
      }

      if (!((WorldGenTreeOrange) object).generate(world, random, x + i1, y, z + j1)) {
        if (flag) {
          world.setBlock(x + i1, y, z + j1, this.blockID, l, 4);
          world.setBlock(x + i1 + 1, y, z + j1, this.blockID, l, 4);
          world.setBlock(x + i1, y, z + j1 + 1, this.blockID, l, 4);
          world.setBlock(x + i1 + 1, y, z + j1 + 1, this.blockID, l, 4);
        } else {
          world.setBlock(x, y, z, this.blockID, l, 4);
        }
      }
    }
  }
}
