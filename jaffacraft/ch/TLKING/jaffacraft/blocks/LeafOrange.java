package ch.TLKING.jaffacraft.blocks;

import java.util.List;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLeaves;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;

import ch.TLKING.jaffacraft.JaffaCraft;
import ch.TLKING.jaffacraft.JaffaCreativeTab;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class LeafOrange extends BlockLeaves {
  private static String name = "leafOrange";

  public LeafOrange() {
    super(3501);
    this.setCreativeTab(JaffaCreativeTab.jaffaTab);
    this.setUnlocalizedName(name);

    this.setHardness(0.2F);
    this.setLightOpacity(1);
    this.setStepSound(soundGrassFootstep);
  }

  public static String getName() {
    return name;
  }

  public static final String[][] leafTypes = new String[][] { { "leaves_orange" }, { "leaves_orange_opaque" } };
  public static final String[]   treeTypes = new String[] { "saplingOrange" };
  private Icon[][]               iconArray = new Icon[2][];
  private int                    iconType;

  @Override
  @SideOnly(Side.CLIENT)
  public void getSubBlocks(int leaf, CreativeTabs creativeTab, List leafList) {
    leafList.add(new ItemStack(JaffaCraft.leafOrange, 1, 0));
  }

  @Override
  @SideOnly(Side.CLIENT)
  public void registerIcons(IconRegister iconRegister) {
    for (int i = 0; i < leafTypes.length; ++i) {
      this.iconArray[i] = new Icon[leafTypes[i].length];

      for (int j = 0; j < leafTypes[i].length; ++j) {
        this.iconArray[i][j] = iconRegister.registerIcon(JaffaCraft.MODID + ":" + leafTypes[i][j]);
      }
    }
  }

  @Override
  @SideOnly(Side.CLIENT)
  public Icon getIcon(int side, int meta) {
    return this.iconArray[this.iconType][0];
  }

  @Override
  public void setGraphicsLevel(boolean par1) {
    this.graphicsLevel = par1;
    this.iconType = par1 ? 0 : 1;
  }

  @Override
  public boolean isOpaqueCube() {
    return false;
  }

  @Override
  @SideOnly(Side.CLIENT)
  public boolean shouldSideBeRendered(IBlockAccess p_149646_1_, int p_149646_2_, int p_149646_3_, int p_149646_4_, int p_149646_5_) {
    return true;
  }

  @Override
  public void beginLeavesDecay(World world, int x, int y, int z) {
    super.beginLeavesDecay(world, x, y, z);
  }

  @Override
  public boolean isFlammable(IBlockAccess world, int x, int y, int z, int metadata, ForgeDirection face) {
    return true;
  }

  @Override
  public int idDropped(int par1, Random par2Random, int par3) {
    return JaffaCraft.saplingOrange.blockID;
  }

  @Override
  public void dropBlockAsItemWithChance(World world, int x, int y, int z, int metadata, float f, int chance) {
    if (!world.isRemote) {
      int j1 = 20;

      if ((metadata & 3) == 3) {
        j1 = 40;
      }

      if (chance > 0) {
        j1 -= 2 << chance;

        if (j1 < 10) {
          j1 = 10;
        }
      }

      if (world.rand.nextInt(j1) == 0) {
        int k1 = this.idDropped(metadata, world.rand, chance);
        this.dropBlockAsItem_do(world, x, y, z, new ItemStack(k1, 1, this.damageDropped(metadata)));
      }

      j1 = 10;

      if ((metadata & 3) == 0 && world.rand.nextInt(j1) == 0) {
        this.dropBlockAsItem_do(world, x, y, z, new ItemStack(JaffaCraft.orange, 1, 0));
      }
    }
  }
}
