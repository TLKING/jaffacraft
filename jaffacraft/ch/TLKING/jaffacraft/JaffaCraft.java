package ch.TLKING.jaffacraft;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.stats.Achievement;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

import ch.TLKING.jaffacraft.Generation.JaffaWorldGeneration;
import ch.TLKING.jaffacraft.blocks.LeafOrange;
import ch.TLKING.jaffacraft.blocks.SaplingOrange;
import ch.TLKING.jaffacraft.events.BoneMealEvent;
import ch.TLKING.jaffacraft.items.ChocolatePowder;
import ch.TLKING.jaffacraft.items.Flour;
import ch.TLKING.jaffacraft.items.JaffaCake;
import ch.TLKING.jaffacraft.items.Mortar;
import ch.TLKING.jaffacraft.items.Orange;
import ch.TLKING.jaffacraft.items.Pestel;
import ch.TLKING.jaffacraft.items.StoneBowl;

@Mod(modid = JaffaCraft.MODID, version = JaffaCraft.VERSION)
public class JaffaCraft {
  public static final String MODID   = "jaffacraft";
  public static final String VERSION = "0.1";

  public static Block        saplingOrange;
  public static Block        leafOrange;

  public static ItemFood     jaffaCake;
  public static ItemFood     orange;

  public static Item         chocolatePowder;
  public static Item         flour;
  public static Item         pestle;
  public static Item         stoneBowl;
  public static Item         mortar;

  @EventHandler
  public void preInit(FMLPreInitializationEvent event) {
    // Create items for the mod
    jaffaCake = new JaffaCake();
    orange = new Orange();
    chocolatePowder = new ChocolatePowder();
    flour = new Flour();
    pestle = new Pestel();
    stoneBowl = new StoneBowl();
    mortar = new Mortar();
    
    // setting individual properties
    mortar.setContainerItem(mortar);

    // Create blocks for the mod
    saplingOrange = new SaplingOrange();
    leafOrange = new LeafOrange();

    // Register blocks, items
    GameRegistry.registerItem(jaffaCake, MODID + JaffaCake.getName());
    GameRegistry.registerItem(orange, MODID + Orange.getName());
    GameRegistry.registerItem(chocolatePowder, MODID + ChocolatePowder.getName());
    GameRegistry.registerItem(flour, MODID + Flour.getName());
    GameRegistry.registerItem(pestle, MODID + Pestel.getName());
    GameRegistry.registerItem(stoneBowl, MODID + StoneBowl.getName());
    GameRegistry.registerItem(mortar, MODID + Mortar.getName());

    GameRegistry.registerBlock(saplingOrange, MODID + SaplingOrange.getName());
    GameRegistry.registerBlock(leafOrange, MODID + LeafOrange.getName());

    // Register world generator
    GameRegistry.registerWorldGenerator(new JaffaWorldGeneration());

    // Register Recepies
    JaffaCake.registerRecepies();
    Mortar.registerRecepies();
    Pestel.registerRecepies();
    StoneBowl.registerRecepies();
    ChocolatePowder.registerRecepies();
    Flour.registerRecepies();
  }

  @EventHandler
  public void load(FMLInitializationEvent event) {
    // Register events
    MinecraftForge.EVENT_BUS.register(new BoneMealEvent());
  }
}
