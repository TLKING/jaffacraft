package ch.TLKING.jaffacraft.Generation;

import java.util.Random;

import ch.TLKING.jaffacraft.Generation.Tree.WorldGenTreeOrange;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.biome.BiomeGenPlains;
import net.minecraft.world.chunk.IChunkProvider;
import cpw.mods.fml.common.IWorldGenerator;

public class JaffaWorldGeneration implements IWorldGenerator {

  @Override
  public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
    switch (world.provider.dimensionId) {
      case -1:
        generateNether(world, random, chunkX * 16, chunkZ * 16);
      break;
      case 0:
        generateSurface(world, random, chunkX * 16, chunkZ * 16);
      break;
      case 1:
        generateEnd(world, random, chunkX * 16, chunkZ * 16);
      break;
    }
  }

  private void generateSurface(World world, Random random, int x, int z) {
    int chunkX;
    int chunkY;
    int chunkZ;
    for (int t = 0; t < 5; t++) {
      chunkX = x + random.nextInt(16);
      chunkY = random.nextInt(90);
      chunkZ = z + random.nextInt(16);

      if (world.getBiomeGenForCoords(chunkX, chunkZ).biomeName == BiomeGenBase.plains.biomeName) {
        (new WorldGenTreeOrange(true)).generate(world, random, chunkX, chunkY, chunkZ);
      }
    }
  }

  private void generateNether(World world, Random random, int i, int j) {

  }

  private void generateEnd(World world, Random random, int i, int j) {

  }

}
